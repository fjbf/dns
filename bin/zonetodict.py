#!/usr/bin/env python3
import sys
import os
from pprint import pprint

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path + "/../libs/python/")
from parsezone import parsezone

if len(sys.argv) != 2:
    print("{} fichero".format(sys.argv[0]))
    quit()
    
try:
    with open(sys.argv[1]) as f:
        lines = [line for line in f]
except FileNotFoundError:
    print("Imposible abrir el fichero")
    quit(1)
pprint(parsezone(lines))