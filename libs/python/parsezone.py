#!/usr/bin/env python
import sys
from pprint import pprint

def parsezone(lines): 
    origin = "."
    zone = {"origin": {".": []}}
    ttl = 0
    multiline = 0
    for line in lines:
            line = line.strip()
            if len(line) == 0:
                # Linea en blanco
                continue
            #print(line)
            # Nos quedamos el comando
            datos = line.split()
            if datos[0][0] == ";":
                continue
            if ")" in line.split(";")[0]:
                    multiline = 0
                    line = m_data + "\n" + line
                    datos = line.split()
                    # continue
                    
            elif multiline == 1:
                m_data = m_data + "\n" + line
                continue
            elif "(" in line or "SOA" in line:
                multiline = 1
                m_data = line
                continue
            
            c = datos[0]
            t = datos[1]
            if  c == "$ORIGIN":
                origin = datos[1]
                if origin not in zone:
                    zone['origin'][origin] = []
            elif c == "$TTL":
                ttl = datos[1]
            elif "SOA" in line:
                soa = line.split("\n")
                zone['soa']= {
                    'soa': soa[0][:-1],
                    'serial': soa[1].split(";")[0].strip(),
                    'refresh': soa[2].split(";")[0].strip(),
                    'retry': soa[3].split(";")[0].strip(),
                    'expire': soa[4].split(";")[0].strip(),
                    'minimum': soa[5].split(";")[0].strip(),
                }
            else:
                if origin != "." and len(datos) == 2:
                    d = [h, datos[0], datos[1]]
                    datos = d
                h = datos[0]
                t = datos[1]
                if len(datos) == 2:
                    zone['origin'][origin].append([h,t])
                    #print("{} {}".format(h,t))
                    pass
                else:    
                    v = " ".join(datos[2:])
                    zone['origin'][origin].append([h, t, v])
                    #print("{} {} {}".format(h,t,v))
                    pass
            #print(multiline)
    return(zone)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("{} fichero".format(sys.argv[0]))
        quit()
    try:
        with open(sys.argv[1]) as f:
            lines = [line for line in f]
    except FileNotFoundError:
        print("Imposible abrir el fichero")
        quit(1)
    pprint(parsezone(lines))